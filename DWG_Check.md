# DWGファイルの登録チェックソフトの作成依頼

追加での依頼。構想中

## 背景

図面の作成完了後、ファイル名を仕様に合わせて変更する。

その後サーバーへの登録となるが、この作業が非常に面倒なので自動化したい。


## 仕様

あるフォルダに以下のファイルが作られている。
これらのファイルを自動でサーバーへ登録したい。


```
1D-UT26763.dwg  
1D-UT26764E.dwg
3D-UT26761.dwg
TR-C712.dwg
4A-NP32795.dwg 
4A-NP34433.dwg
4A-NP35935.dwg
4A-NP35935.ipt
1D-UT26763.iam
```

### 操作方法

検討中