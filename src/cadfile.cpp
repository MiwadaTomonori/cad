#include "cadfile.hpp"
#include <tuple>
#include <cctype>
#include <algorithm>
#include <string>

namespace cadfile
{
    using namespace std;

    class ErrorMessage
    {
        stringstream s;
        bool fail;

    public:
        ErrorMessage() : s(""), fail(false) {}
        operator bool() const noexcept { return fail; }

        template <typename T>
        ErrorMessage &operator<<(T v)
        {
            fail = true;
            s << v;
            return *this;
        }

        string message(string_view source)
        {
            s << ": '" << source << "'";
            return s.str();
        }
    };

    string uppercase(string_view sview)
    {
        string s(sview);
        transform(s.begin(), s.end(), s.begin(),
                  [](unsigned char c) { return toupper(c); });
        return s;
    }

    static inline bool startsWith(string_view text, string_view start)
    {
        return text.length() > start.length() && text.compare(0, start.length(), start) == 0;
    }

    /**
     * @brief 仕様バージョンを判断する。
     * @brief text 文字列(大文字)
     * @return 分からない場合は0になる。
     */
    static uint8_t get_version(string_view text)
    {
        const auto len = text.length();
        if (len == 0)
            return 0;
        if (text[len - 2] == '-')
        {
            if (startsWith(text, "TR-") || startsWith(text, "*TR-"))
                return 3;
            return 2;
        }
        return 1;
    }

    /**
     * @brief version 2のヘッダを取得する
     * @param text テキスト
     * @param version get_versionで得たバージョン
     * @param emsg エラー情報を格納するストリーム
     * @return (rest, header)。restはtextからheader部分を削除したもの。
     */
    static tuple<string_view, string_view> get_header(
        string_view text,
        uint8_t version,
        ErrorMessage &emsg)
    {
        if (text[0] == '*' || version == 3)
        {
            if (version == 1)
            {
                emsg << "Version is " << (uint32_t)version << ". But `*` is found.";
                return {text, ""};
            }
            auto index = text.find('-');
            if (index < 0 || index == text.length() - 1)
            {
                emsg << "header hyphen not found.";
                return {text, ""};
            }

            auto header = text.substr(0, index);
            text = text.substr(index + 1);
            return {text, header};
        }
        return {text, ""};
    }
    /**
     * @brief cad_typeが対応している文字列か判定する
     * @param cad_type A,V,D,S(大文字)
     * @param version バージョン
     * @param emsg エラー情報を格納するストリーム
     */
    static void check_cad_type(
        char cad_type,
        uint8_t version,
        ErrorMessage &emsg)
    {
        if (cad_type != 'A' &&
            cad_type != 'V' &&
            cad_type != 'D' &&
            cad_type != 'S')
            emsg << "Unsupported type '" << cad_type << "'. ";
        else if (version == 1 && (cad_type != 'A' && cad_type != 'V'))
            emsg << "Unsupported v1 type '" << cad_type << "'. ";

        else if (version == 2 && (cad_type != 'D' && cad_type != 'S'))
            emsg << "Unsupported v2 type '" << cad_type << "'. ";
    }

    /**
     * @brief cad sizeが1～4か判断する
     * @param emsg エラー情報を格納するストリーム
     */
    static void check_cad_size(uint8_t cad_size, uint8_t version, ErrorMessage &emsg)
    {
        if (!(1 <= cad_size && cad_size <= 4))
            emsg << "Unsupported size '" << (char)(cad_size + '0') << "'. ";
    }

    static tuple<string_view, uint8_t, char> parseType(string_view rest, uint8_t version, ErrorMessage &emsg)
    {
        const uint8_t cad_size = rest[0] - '0';
        const auto cad_type = rest[1];
        check_cad_size(cad_size, version, emsg);
        check_cad_type(cad_type, version, emsg);
        return {rest.substr(3), cad_size, cad_type};
    }

    /**
     * @brief 図面サイズ以降（含む）の文字列から、仕様1の情報を構築する
     * @param src 図面サイズ以降（含む）の文字列
     * @param cad_size 1～4
     * @param cad_type A,V
     * @param out 出力
     * @param emsg エラー情報を格納するストリーム
     */
    static void parseAfterHyphenV1(
        string_view src,
        GitCadFileNumberingV1 &out,
        ErrorMessage &emsg)
    {

        auto [rest, cad_size, cad_type] = parseType(src, 1, emsg);
        if (emsg)
            return;

        constexpr size_t MAX_LENGTH = 2 + 5 + 1 + 1;
        string_view description = "";
        if (rest.length() == MAX_LENGTH)
        {
            description = rest.substr(0, 2);
            rest = rest.substr(2);
        }

        constexpr size_t LENGTH = 5 + 1 + 1;
        if (rest.length() != LENGTH)
        {
            emsg << "Illegal text length.";
            return;
        }
        string_view numbering = rest.substr(0, 5);
        string_view specification = rest.substr(5, 1);
        string_view revision = rest.substr(6, 1);
        out = GitCadFileNumberingV1{
            numbering,
            revision,
            // --------
            cad_size,
            cad_type,
            description,
            specification,
        };
    }

    /**
     * @brief 図面サイズ以降（サイズ含む）文字列から、仕様2の情報を構築する
     * @param src サイズから
     * @param header ヘッダ
     * @param cad_size 1～4
     * @param cad_type A,V
     * @param out 出力
     * @param emsg エラー情報を格納するストリーム
     * @return 成功したかどうか
     */
    static void parseAfterHyphenV2(
        string_view src,
        string_view header,
        GitCadFileNumberingV2 &out,
        ErrorMessage &emsg)
    {

        auto [rest, cad_size, cad_type] = parseType(src, 2, emsg);
        if (emsg)
            return;

        string_view revision = rest.substr(rest.length() - 1, 1);
        rest = rest.substr(0, rest.length() - 2);

        const size_t FULL_LENGTH = 2 + 5 + 1;
        const auto len = rest.length();
        if (len == FULL_LENGTH)
            out = GitCadFileNumberingV2{
                rest.substr(2, 5), // numbering
                revision,
                // --------
                cad_size,
                cad_type,

                header,
                rest.substr(0, 2),           // description
                (char)toupper(rest[len - 1]) // language
            };
        else if (len == FULL_LENGTH - 2)
            out = GitCadFileNumberingV2{
                rest.substr(0, 5), // numbering
                revision,
                // --------
                cad_size,
                cad_type,
                header,
                "",                          // description
                (char)toupper(rest[len - 1]) // language
            };
        else if (len == FULL_LENGTH - 1)
            out = GitCadFileNumberingV2{
                rest.substr(2, 5), // numbering
                revision,
                // --------
                cad_size,
                cad_type,
                header,
                rest.substr(0, 2), // description
                GitCadFileNumberingV2::JAPANESE,
            };
        else if (len == FULL_LENGTH - 3)
            out = GitCadFileNumberingV2{
                rest, // numbering
                revision,
                // --------
                cad_size,
                cad_type,
                header,
                "", // description
                GitCadFileNumberingV2::JAPANESE,
            };
        else
            emsg << "Illegal text length.";
    }

    /**
     * @brief ヘッダ以降（ヘッダ含まず）文字列から、仕様3の情報を構築する
     * @param src ヘッダ以降（ヘッダ含まず）文字列
     * @param header ヘッダ
     * @param cad_size 1～4
     * @param cad_type A,V
     * @param out 出力
     * @param emsg エラー情報を格納するストリーム
     * @return 成功したかどうか
     */
    static void parseAfterHyphenV3(
        string_view rest,
        string_view header,
        GitCadFileNumberingV3 &out,
        ErrorMessage &emsg)
    {
        if (rest.length() != 6)
        {
            emsg << "Illegal text length.";
            return;
        }
        string_view revision = rest.substr(rest.length() - 1, 1);
        out = GitCadFileNumberingV3{
            rest.substr(0, 4), // numbering
            revision,
            // --------
            header,
        };
    }

    // ------------------------------------------------------------------
    CadFileNumbering::
        CadFileNumbering(string_view name) : source(uppercase(name)),
                                             _version(0),
                                             _numbering({})
    {
        if (source.length() == 0)
            throw parse_exception("empty string");

        const auto version = get_version(source);
        _version = version;

        ErrorMessage emsg;

        if (version == 0)
        {
            emsg << "Unsupported syntax.";
            throw parse_exception(emsg.message(source));
        }

        constexpr size_t MIN_LENGTH = 9;
        if (source.length() < MIN_LENGTH)
        {
            emsg << "Illegal text length.";
            throw parse_exception(emsg.message(source));
        }

        auto [rest, header] = get_header(source,
                                         version,
                                         emsg);
        if (emsg)
            throw parse_exception(emsg.message(source));

        switch (version)
        {
        case 1:
            parseAfterHyphenV1(rest,
                               _numbering.v1,
                               emsg);
            break;
        case 2:
            parseAfterHyphenV2(rest,
                               header,
                               _numbering.v2,
                               emsg);
            break;

        case 3:
            parseAfterHyphenV3(rest,
                               header,
                               _numbering.v3,
                               emsg);
            break;

        default:
            emsg << "FATAL ERROR: not implemented for version "
                 << (uint32_t)version
                 << ". Please denounce the author of this program.";
        }
        if (emsg)
            throw parse_exception(emsg.message(source));
    }

    // ---------------------------------------------------------------------
    // getDWGFilePath
    // ---------------------------------------------------------------------

    static void initPathResult(stringstream &ss, string_view base)
    {
        const auto len = base.length();
        if (len == 0)
            return;
        if (base[len - 1] == PATH_SEP)
            ss << base;
        else
            ss << base << PATH_SEP;
    }

    static string getDWGFilePathV1(
        const CadFileNumbering &n,
        string_view base,
        string_view exp)
    {
        const auto &v = n.asV1();
        stringstream result;
        initPathResult(result, base);

        switch (v.cad_type)
        {
        case 'A':
            result << "RHA";
            break;
        case 'V':
            result << "RKV";
            break;
        default:
            throw invalid_argument("Invalid CadFileNumbering object.");
        }
        const auto num = v.numbering;
        if (num.length() != 5)
            throw invalid_argument("Invalid CadFileNumbering object.");

        result << num.substr(0, 3) << PATH_SEP
               << (uint32_t)v.cad_size << v.cad_type << '-' << num << exp;

        return result.str();
    }

    static string getDWGFilePathV2(
        const CadFileNumbering &n,
        string_view base,
        string_view exp)
    {
        const auto v = n.asV2();
        stringstream result;
        initPathResult(result, base);

        switch (v.cad_type)
        {
        case 'D':
            result << "RHD";
            break;
        case 'S':
            result << "RKS";
            break;
        default:
            throw invalid_argument("Invalid CadFileNumbering object.");
        }
        const auto num = v.numbering;
        if (num.length() != 5)
            throw invalid_argument("Invalid CadFileNumbering object.");

        result << num.substr(0, 3) << PATH_SEP
               << (uint32_t)v.cad_size << v.cad_type << '-' << num;
        if (v.language != GitCadFileNumberingV2::JAPANESE)
            result << v.language;
        result << exp;

        return result.str();
    }

    static string getDWGFilePathV3(
        const CadFileNumbering &n,
        string_view base,
        string_view exp)
    {
        const auto v = n.asV3();
        stringstream result;
        initPathResult(result, base);

        const auto num = v.numbering;
        if (num.length() != 4)
            throw invalid_argument("Invalid CadFileNumbering object.");

        result << "TR-" << num.substr(0, 2) << PATH_SEP
               << "TR-" << num << exp;

        return result.str();
    }

    string getDWGFilePath(
        const CadFileNumbering &n,
        string_view base,
        string_view exp)
    {
        switch (n.getVersion())
        {
        case 0:
            throw std::invalid_argument("Invalid cad file numbering object.");
        case 1:
            return getDWGFilePathV1(n, base, exp);
        case 2:
            return getDWGFilePathV2(n, base, exp);
        case 3:
            return getDWGFilePathV3(n, base, exp);
        }
        stringstream emsg;
        emsg << "FATAL ERROR: not implemented for version "
             << (uint32_t)n.getVersion()
             << ". Please denounce the author of this program.";
        throw std::invalid_argument(emsg.str());
    }

}