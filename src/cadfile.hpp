#include <string>
#include <string_view>
#include <sstream>
#include <stdexcept>

namespace cadfile
{
#ifdef _WIN32
    constexpr const char *DEFAULT_DIRECTORY_PATH = "\\\\dev-server\\cad";
    constexpr const char PATH_SEP = '\\';
#else
    constexpr const char *DEFAULT_DIRECTORY_PATH = "//dev-server/cad";
    constexpr const char PATH_SEP = '/';
#endif

    /**
     * @brief パースに失敗したことを示す例外
     */
    class parse_exception : public std::exception
    {
        std::string msg;

    public:
        parse_exception(const std::string &msg) : msg(msg) {}
        parse_exception(std::string &&msg) : msg(msg) {}
        virtual ~parse_exception() throw (){}
        virtual const char *what() { return msg.c_str(); }
    };

    /**
     * @brief 採番の共通項
     */
    struct GitCadFileNumberingBase
    {
        std::string_view numbering;
        std::string_view revision;
    };
    /**
     * @brief 採番の仕様1
     */
    struct GitCadFileNumberingV1 : public GitCadFileNumberingBase
    {
        uint8_t cad_size;
        char cad_type;
        std::string_view description;
        std::string_view specification;
    };

    /**
     * @brief 採番の仕様2
     */
    struct GitCadFileNumberingV2 : public GitCadFileNumberingBase
    {
        static constexpr char ENGLISH = 'E';
        static constexpr char JAPANESE = 'J';
        uint8_t cad_size;
        char cad_type;
        std::string_view header;
        std::string_view description;
        char language;
    };

    /**
     * @brief 採番の仕様3
     */
    struct GitCadFileNumberingV3 : public GitCadFileNumberingBase
    {
        std::string_view header;
    };

    /**
     * @brief 採番
     */
    class CadFileNumbering
    {
    private:
        /**
        *  @brief 採番のデータ
        */
        union GitCadFileNumbering
        {
            GitCadFileNumberingBase base;
            GitCadFileNumberingV1 v1;
            GitCadFileNumberingV2 v2;
            GitCadFileNumberingV3 v3;
        };

        std::string source;
        uint8_t _version;
        GitCadFileNumbering _numbering;

    public:
        /**
         * @param name 採番文字列
         * @throws parse_exception パースに失敗した場合に発生
         */
        CadFileNumbering(std::string_view name);
        //! @brief 採番の仕様番号を返す
        uint8_t getVersion() const noexcept { return _version; }
        //! @brief コンストラクタで渡された文字列
        const std::string &getSource() const noexcept { return source; }
        //! @brief 仕様1として取得する。getVersion()が1でない場合はruntime_error
        const GitCadFileNumberingV1 &asV1() const
        {
            if (_version == 1)
                return _numbering.v1;
            std::stringstream ss;
            ss << "Version: expected 1, actural " << (uint32_t)_version;
            throw std::runtime_error(ss.str());
        }
        //! @brief 仕様2として取得する。getVersion()が2でない場合はruntime_error
        const GitCadFileNumberingV2 &asV2() const
        {
            if (_version == 2)
                return _numbering.v2;
            std::stringstream ss;
            ss << "Version: expected 2, actural " << (uint32_t)_version;
            throw std::runtime_error(ss.str());
        }
        //! @brief 仕様3として取得する。getVersion()が3でない場合はruntime_error
        const GitCadFileNumberingV3 &asV3() const
        {
            if (_version == 3)
                return _numbering.v3;
            std::stringstream ss;
            ss << "Version: expected 3, actural " << (uint32_t)_version;
            throw std::runtime_error(ss.str());
        }
        //! @brief 図面番号を返す
        std::string_view getNumbering() const noexcept { return _numbering.base.numbering; }
        //! @brief 改定番号を返す
        std::string_view getRivision() const noexcept { return _numbering.base.revision; }
    };

    /**
     * @brief 採番から、.dwgファイルのパス文字列を生成する
     * @param numbering 採番
     * @param base ルートディレクトリ。DEFAULT_DIRECTORY_PATHを使うと良い
     * @return パス。
     */
    std::string getDWGFilePath(const CadFileNumbering &numbering, std::string_view base, std::string_view extension);

    inline std::string getDWGFilePath(const CadFileNumbering &numbering, std::string_view base)
    {
        return getDWGFilePath(numbering, base, ".dwg");
    }

    /**
     * @brief 採番から、.dwgファイルのパス文字列を生成する
     * @param numbering 採番
     * @return パス。
     */
    inline std::string getDWGFilePath(const CadFileNumbering &numbering)
    {
        return getDWGFilePath(numbering, DEFAULT_DIRECTORY_PATH);
    }
    /**
     * @brief 採番から、.dwgファイルのパス文字列を生成する
     * @param numbering 採番
     * @param base ルートディレクトリ。DEFAULT_DIRECTORY_PATHを使うと良い
     * @return パス。
     */
    inline std::string getDWGFilePath(std::string_view numbering, std::string_view base, std::string_view extension)
    {
        return getDWGFilePath(CadFileNumbering(numbering), base, extension);
    }

    /**
     * @brief 採番から、.dwgファイルのパス文字列を生成する
     * @param numbering 採番
     * @param base ルートディレクトリ。DEFAULT_DIRECTORY_PATHを使うと良い
     * @return パス。
     */
    inline std::string getDWGFilePath(std::string_view numbering, std::string_view base)
    {
        return getDWGFilePath(CadFileNumbering(numbering), base);
    }

    /**
     * @brief 採番から、.dwgファイルのパス文字列を生成する
     * @param numbering 採番
     * @return パス。
     */
    inline std::string getDWGFilePath(std::string_view numbering)
    {
        return getDWGFilePath(CadFileNumbering(numbering), DEFAULT_DIRECTORY_PATH);
    }
}