
#include <iostream>
#include <filesystem>
#include <string>
#include <string_view>
#include <vector>

#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

#include "cadfile.hpp"

// CMakeからGitから取得して渡したいけど、めんどうだった
#ifndef CADFILE_VERSION
#  define CADFILE_VERSION "1.0.0"
#endif

// これはCMakeからGitから取得して渡しています
#ifndef GIT_COMMIT_ID
#  define GIT_COMMIT_ID ""
#endif

//! @brief ヘルプメッセージを標準出力に出します
void showHelp()
{
    std::cout << R"(cadfile [NAME [NAME]] [-d DIR] [-e EXT] [-h] [-p] [-v]

Cad file open program version )" CADFILE_VERSION GIT_COMMIT_ID R"(

Argument:
    NAME  : numbering name.
            You can input multiple numbers.
            If you do not input numberings as arguments,
            you input it on standard input.

Option:
    -e EXT: file extension. Default is '.dwg'
    -d DIR: server directory. Default is ')"
              << cadfile::DEFAULT_DIRECTORY_PATH << R"('.
    -h    : show this text
    -p    : show path only (not open files)
    -v    : show version
)" << std::endl;
}

//! @brief バージョン情報を標準出力に出します
void showVersion()
{
    std::cout << "cadfile " CADFILE_VERSION GIT_COMMIT_ID << std::endl;
}

/**
 * @brief このプログラムのオプションや引数の解析
 */
struct Option
{
    bool version;
    bool help;
    bool err;
    bool path_only;
    std::vector<std::string> inputFiles;
    std::string_view ext;
    std::string_view baseDir;

    Option(int argc, char const *argv[]) : version(false),
                                           help(false),
                                           err(false),
                                           path_only(false),
                                           inputFiles(),
                                           ext(".dwg"),
                                           baseDir(cadfile::DEFAULT_DIRECTORY_PATH)
    {
        for (auto i = 1; i < argc; i++)
        {
            std::string_view arg = argv[i];
            if (arg == "-h")
            {
                help = true;
                return;
            }
            else if (arg == "-e")
            {
                i++;
                if (i == argc)
                {
                    help = true;
                    err = true;
                    return;
                }
                ext = argv[i];
            }
            else if (arg == "-d")
            {
                i++;
                if (i == argc)
                {
                    help = true;
                    err = true;
                    return;
                }
                baseDir = argv[i];
            }
            else if (arg == "-v")
                version = true;
            else if (arg == "-p")
                path_only = true;
            else
                inputFiles.emplace_back(arg);
        }
    }

    //! @brief 標準入力から、引数を受け取ります
    void inputFromStdin()
    {
        std::cout << "Input numberings >";
        std::cout.flush();
        std::string trimchar = " 　";
        while (true)
        {
            std::string buf;
            getline(std::cin, buf);
            // trim
            buf.erase(0, buf.find_first_not_of(trimchar));
            buf.erase(buf.find_last_not_of(trimchar) + 1);

            if (buf.length() == 0)
                break;

            inputFiles.push_back(move(buf));
        }
    }
    //! @brief 引数（採番）が空かどうかを返します
    bool empty() { return inputFiles.size() == 0; }
};

//! @brief パースして、ファイルをShellExecuteで起動する
void _main(const Option &opt)
{
    namespace fs = std::filesystem;
    for (const auto &i : opt.inputFiles)
    {
        try
        {
            const auto pathstr = cadfile::getDWGFilePath(i, opt.baseDir, opt.ext);
            if (opt.path_only)
                std::cout << pathstr << std::endl;
            else if (fs::exists(fs::path(pathstr)))
                ::ShellExecuteA(NULL, "Open", pathstr.c_str(), NULL, NULL, SW_SHOW);
            else
                std::cerr << "'" << pathstr << "' is not found." << std::endl;
        }
        catch (cadfile::parse_exception &e)
        {
            std::cerr << "Error: " << e.what() << std::endl;
        }
    }
}

//! @brief ルート関数
int main(int argc, char const *argv[])
{
    try
    {
        Option opt(argc, argv);

        if (opt.help)
        {
            showHelp();
            return opt.err ? 1 : 0;
        }

        if (opt.version)
        {
            showVersion();
            return 0;
        }

        if (opt.empty())
            opt.inputFromStdin();

        if (opt.empty())
            return 0;

        _main(opt);
        return 0;
    }
    catch (std::exception &e)
    {
        std::cerr << "Unexpected Error:" << e.what() << std::endl;
    }
    catch (const char *e)
    {
        std::cerr << "Unexpected Error:" << e << std::endl;
    }
    catch (...)
    {
        std::cerr << "Unexpected Error Occured" << std::endl;
    }
    return 1;
}
