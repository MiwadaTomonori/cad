
#include <tuple>
#include <string>
#include <string_view>
#include <iostream>
#include <algorithm>

#include "gtest/gtest.h"
#include "../src/cadfile.hpp"
using namespace cadfile;

inline std::string toLower(std::string s)
{
    std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c) { return std::tolower(c); });
    return s;
}

inline std::tuple<std::string_view,
           std::string_view,
           std::string_view,
           uint8_t,
           char,
           std::string_view,
           std::string_view>
v1(
    std::string_view source,
    std::string_view numbering,
    std::string_view revision,
    uint8_t cad_size,
    char cad_type,
    std::string_view desc,
    std::string_view specification)
{
    return {source, numbering, revision, cad_size, cad_type, desc, specification};
}

inline std::tuple<std::string_view,
           std::string_view,
           std::string_view,
           std::string_view,
           uint8_t,
           char,
           std::string_view,
           char>
v2(
    std::string_view source,
    std::string_view numbering,
    std::string_view revision,
    std::string_view head,
    uint8_t cad_size,
    char cad_type,
    std::string_view desc,
    char lang)
{
    return {source, numbering, revision, head, cad_size, cad_type, desc, lang};
}

inline std::tuple<std::string_view,
           std::string_view,
           std::string_view,
           std::string_view>
v3(
    std::string_view source,
    std::string_view numbering,
    std::string_view revision,
    std::string_view head)
{
    return {source, numbering, revision, head};
}



TEST(CadFileNumberingTest, parseV1)
{
    const auto list = {
        v1("3A-FR5823310", "58233", "0", 3, 'A', "FR", "1"),
        v1("3A-5823310"  , "58233", "0", 3, 'A',   "", "1"),
        v1("3A-FR5823320", "58233", "0", 3, 'A', "FR", "2"),
        v1("2V-M94779013", "47790", "3", 2, 'V', "M9", "1"),
        v1("4V-M94779014", "47790", "4", 4, 'V', "M9", "1"),
        v1("1V-4779014"  , "47790", "4", 1, 'V',   "", "1"),
    };
    for (auto [source, numbering, revision, cad_size, cad_type, desc, specification] : list)
    {
        auto low = toLower(std::string(source));
        CadFileNumbering n(source);
        CadFileNumbering n2(low);
        EXPECT_EQ(1, n.getVersion());
        EXPECT_EQ(1, n2.getVersion());
        EXPECT_EQ(numbering, n.getNumbering());
        EXPECT_EQ(numbering, n2.getNumbering());
        EXPECT_EQ(revision, n.getRivision());
        EXPECT_EQ(revision, n2.getRivision());

        auto &v = n.asV1();
        auto &v2 = n2.asV1();
        EXPECT_EQ(cad_size, v.cad_size);
        EXPECT_EQ(cad_size, v2.cad_size);
        EXPECT_EQ(cad_type, v.cad_type);
        EXPECT_EQ(cad_type, v2.cad_type);
        EXPECT_EQ(desc, v.description);
        EXPECT_EQ(desc, v2.description);
        EXPECT_EQ(specification, v.specification);
        EXPECT_EQ(specification, v2.specification);
    }
}


TEST(CadFileNumberingTest, parseV2)
{
    const auto list = {
        v2("*M-3D-UT18313-1", "18313", "1", "*M", 3, 'D', "UT", 'J'),
        v2("3D-UT18313-1", "18313", "1", "", 3, 'D', "UT", 'J'),
        v2("*M-3D-UT18313E-1", "18313", "1", "*M", 3, 'D', "UT", 'E'),
        v2("*M-3D-UT18314E-1", "18314", "1", "*M", 3, 'D', "UT", 'E'),
        v2("3D-UT18314E-1", "18314", "1", "", 3, 'D', "UT", 'E'),
        v2("3D-18314E-1", "18314", "1", "", 3, 'D', "", 'E'),
        v2("3D-18314-1", "18314", "1", "", 3, 'D', "", 'J'),
    };
    for (auto [source, numbering, revision, head, cad_size, cad_type, desc, lang] : list)
    {
        auto low = toLower(std::string(source));
        CadFileNumbering n(source);
        CadFileNumbering n2(low);
        EXPECT_EQ(2, n.getVersion());
        EXPECT_EQ(2, n2.getVersion());
        EXPECT_EQ(numbering, n.getNumbering());
        EXPECT_EQ(numbering, n2.getNumbering());
        EXPECT_EQ(revision, n.getRivision());
        EXPECT_EQ(revision, n2.getRivision());

        auto &v = n.asV2();
        auto &v2 = n2.asV2();
        EXPECT_EQ(head, v.header);
        EXPECT_EQ(head, v2.header);
        EXPECT_EQ(cad_size, v.cad_size);
        EXPECT_EQ(cad_size, v2.cad_size);
        EXPECT_EQ(cad_type, v.cad_type);
        EXPECT_EQ(cad_type, v2.cad_type);
        EXPECT_EQ(desc, v.description);
        EXPECT_EQ(desc, v2.description);
        EXPECT_EQ(lang, v.language);
        EXPECT_EQ(lang, v2.language);
    }
}

TEST(CadFileNumberingTest, parseV3)
{
    const auto list = {
        v3("*TR-7884-0", "7884", "0", "*TR"),
        v3("TR-7884-0", "7884", "0", "TR"),
    };
    for (auto [source, numbering, revision, head] : list)
    {
        auto low = toLower(std::string(source));
        CadFileNumbering n(source);
        CadFileNumbering n2(low);
        EXPECT_EQ(3, n.getVersion());
        EXPECT_EQ(3, n2.getVersion());
        EXPECT_EQ(numbering, n.getNumbering());
        EXPECT_EQ(numbering, n2.getNumbering());
        EXPECT_EQ(revision, n.getRivision());
        EXPECT_EQ(revision, n2.getRivision());

        auto &v = n.asV3();
        auto &v2 = n2.asV3();
        EXPECT_EQ(head, v.header);
        EXPECT_EQ(head, v2.header);
    }
}


#ifdef _WIN32
# define SEP "\\"
#else
# define SEP "/"
#endif

TEST(CadFileNumberingTest, dwg)
{
    // v1
    EXPECT_EQ(
        "foobar" SEP "RHA582" SEP "3A-58233.dwg",
        getDWGFilePath("3A-FR5823310", "foobar"));
#ifdef _WIN32
    EXPECT_EQ(
        "\\\\dev-server\\cad\\RHA582\\3A-58233.dwg",
        getDWGFilePath("3A-FR5823310"));
#else
    EXPECT_EQ(
        "//dev-server/cad/RHA582/3A-58233.dwg",
        getDWGFilePath("3A-FR5823310"));
#endif

    EXPECT_EQ(
        "foobar" SEP "RKV082" SEP "1V-08233.dwg",
        getDWGFilePath("1V-FR0823310", "foobar" SEP));

    // v2
    EXPECT_EQ(
        "foobar" SEP "RHD183" SEP "3D-18313.dwg",
        getDWGFilePath("*M-3D-UT18313-1", "foobar"));

    EXPECT_EQ(
        "foobar" SEP "RHD183" SEP "3D-18313E.dwg",
        getDWGFilePath("*M-3D-UT18313E-1", "foobar" SEP));

    EXPECT_EQ(
        "foobar" SEP "RKS083" SEP "2S-08313E.dwg",
        getDWGFilePath("*M-2S-UT08313E-1", "foobar" SEP));

    // v3
    EXPECT_EQ(
        "foobar" SEP "TR-78" SEP "TR-7884.dwg",
        getDWGFilePath("*TR-7884-0", "foobar"));

    EXPECT_EQ(
        "foobar" SEP "TR-08" SEP "TR-0884.dwg",
        getDWGFilePath("TR-0884-0", "foobar" SEP));
}
