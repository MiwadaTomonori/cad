/**
 * サーバーに対してガンガン負荷かけるテストをするわけには行かないので、
 * 単一実行ファイルにしてテストを行う。
 * あと、サーバーがないとテストが通らないのもそれはそれで問題なので。
 *
 * やることは、単純にサーバにファイルがあるはずの入力に対し、
 * 実際に確認するだけ
 * 十分な数で試してください
 *
 * 入力: 採番文字列
 * 出力: OK/Fail (OKの場合は0、Failの場合は1、引数がおかしい場合は2)
 */
#include <iostream>
#include <filesystem>
#include <string>
#include <string_view>

#include "../src/cadfile.hpp"


int main(int argc, char const *argv[])
{
    using namespace std;
    if (argc != 2)
    {
        std::cout << "Invalid argument" << std::endl;
        return 2;
    }

    string_view arg = argv[1];
    auto pathstr = cadfile::getDWGFilePath(arg);
    std::cout << pathstr << std::endl;
    filesystem::path p(pathstr);
    if (filesystem::exists(p))
    {
        std::cout << "OK" << std::endl;
        return 0;
    }
    std::cout << "Fail" << std::endl;
    return 1;
}
